import * as ReactDom from 'react-dom'
import SearchPage from './searchpage'
import LoginPage, { ProtectedRoutes, NonProtectedRoutes } from './loginPage'
import FirstTime, { NonFirstTimeRoutes, FirstTimeRoutes } from './firstTime'
import NotFound from './404'
import ServerError, { AvailableRoutes } from './500'
import { BrowserRouter as Router, Navigate, Route, Routes }
  from 'react-router-dom'
import 'bulma/css/bulma.css'
import './css/table.css'
import './css/bulma-loading.css'
import '@mdi/font/css/materialdesignicons.min.css'



ReactDom.render(
  <div>
    <Router basename="/piwatch">
      <Routes>
        <Route path="500" element={<ServerError />} />
        <Route path="404" element={<NotFound />} />
        <Route path="*" element={<Navigate to="/404" />} />
        <Route element={<AvailableRoutes navigate="/500" />}>
          <Route element={<FirstTimeRoutes navigate="/404" />} >
            <Route path="register" element={<FirstTime />} />
          </Route>
          <Route element={<NonFirstTimeRoutes navigate="/register" />}>
            <Route element={<ProtectedRoutes navigate="/login" />} >
              <Route path="search" element={<SearchPage />} />
            </Route>
            <Route path="/" element={<Navigate to="/login" />} />
            <Route element={<NonProtectedRoutes navigate="/search" />} >
              <Route path="login" element={<LoginPage />} />
            </Route>
          </Route>
        </Route>
      </Routes>
    </Router>
  </div >

  , document.getElementById("root"))
