import { Axios, HeadersDefaults } from 'axios'
import Cookies from 'js-cookie'

export interface HeaderProps extends HeadersDefaults {
  'x-access-token': string;
}


const http = new Axios({
  baseURL: `${window.location.protocol}//${window.location.hostname}:${window.location.port}`,
  headers: {
    "Content-type": "application/json",
    'x-access-token': Cookies.get('access-token') || ""
  },
});


export default http;
