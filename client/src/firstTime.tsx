import RegisterForm from './components/firstTimeForm'
import React from 'react'
import Footer from './components/footer'
import { Navigate, Link } from 'react-router-dom';
import { firstTimeCheck } from './auth'
import AbstractRouteMask from './components/routemask'


export class NonFirstTimeRoutes extends AbstractRouteMask {
  constructor(props: { navigate: string }) {
    super(props);
    this.handler();
  }
  handler = async () => {
    if (await firstTimeCheck()) {
      this.setState({ avail: false, ready: true });
    } else {
      this.setState({ avail: true, ready: true });
    }
  }
}
export class FirstTimeRoutes extends AbstractRouteMask {
  constructor(props: { navigate: string }) {
    super(props);
    this.handler();
  }
  handler = async () => {
    if (await firstTimeCheck()) {
      this.setState({ avail: true, ready: true });
    } else {
      this.setState({ avail: false, ready: true });
    }
  }
}

export default class FirstTimePage extends React.Component<{}, {
  redirect: boolean,
  notFound: boolean
}> {
  registerHandler = () => {
    this.setState({ redirect: true, notFound: false })
  }

  constructor(props: {}) {
    super(props);
    // set it to true first so as not to render login form if already authenticated
    this.state = { redirect: false, notFound: false };
    this.registerHandler = this.registerHandler.bind(this);
  }
  componentDidMount() {
    document.title = 'Register.';
  }
  printSuccess() {
    if (this.state.redirect) {
      return (
        <p className="container is-size-5 has-text-centered" >
          Registeration successful! Use this link to go to&nbsp;
          <Link to="/login">
            login.
          </Link>
        </p>
      )
    }
    return (<div> </div>);
  }
  printInfo() {
    if (!this.state.redirect) {
      return (
        <p className="container is-size-5 has-text-centered has-text-info" >
          It seems that this is the first time you have used this app. To proceed you must get yourself an account. <br />
          Register below.
        </p>
      )
    }
    return (<div> </div>);
  }

  render() {
    if (this.state.notFound) {
      return (
        <Navigate to="/404" />
      )
    }
    else {
      return (
        <div id="main">
          <div className="basicLayoutMain">
            <div className="container my-6">
              <div className="field">
                <span className="icon-text ">
                  <span className="icon is-large">
                    <i className="mdi mdi-48px mdi-account-plus " />
                  </span>
                  <span className="is-size-3"> Registeration </span>
                </span>
                {this.printInfo()}
              </div>
              <div className="box my-3 mx-3 py-4 px-4">
                <RegisterForm ButtonText="Register" submissionHandler={this.registerHandler} />
              </div>
              {this.printSuccess()}
            </div>
          </div>
          <Footer />
        </div>
      )
    }
  }
}
