import React from "react";
import Footer from "./components/footer"
import AbstractRouteMask from "./components/routemask";
import { checkAvailability } from "./auth"

export default class ServerError extends React.Component<{}>{
  componentDidMount() {
    document.title = '500.';
  }

  render() {
    return (
      <div id="main">
        <div className="basicLayoutMain">
          <section className="hero is-danger is-medium">
            <div className="hero-body">
              <p className="title is-size-1">
                500.
              </p>
              <p className="subtitle">
                There seems to be some error while communication with the backend. Call someone for help. <br />
                Maybe report an issue <a className="has-text-link" href="https://gitlab.com/crestfalln/piwatch-website/issues"> here</a>?
              </p>
            </div>
          </section>
        </div>
        <Footer />
      </div>
    )
  }

}


export class AvailableRoutes extends AbstractRouteMask {
  handler = async () => {
    try {
      if (await checkAvailability()) {
        this.setState({ avail: true, ready: true });
      } else {
        this.setState({ avail: false, ready: true });
      }
    } catch (error: any) {
      if (error.request) {
        this.setState({ avail: false, ready: true })
      } else {
        console.log(error);
      }
    }
  }
  constructor(props: { navigate: string }) {
    super(props);
    this.handler();
  }
}


