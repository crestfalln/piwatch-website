import React from "react";
import Footer from "./components/footer"
import { Link } from 'react-router-dom'

export default class NotFound extends React.Component<{}>{
  componentDidMount() {
    document.title = '404.';
  }

  render() {
    return (
      <div id="main">
        <div className="basicLayoutMain">
          <section className="hero is-danger is-medium">
            <div className="hero-body">
              <p className="title is-size-1">
                404.
              </p>
              <p className="subtitle">
                The page you are trying to visit does not exist.
              </p>
            </div>
          </section>
          <div className="container my-6 has-text-left has-text-link">
            <div className="box mx-1 my-6">
              <span className="is-size-4 has-text-weight-bold">
                You may visit these other pages though.
              </span>
              <div className="block" />
              <span className="is-size-5 is-family-code has-text-weight-bold">
                <Link to="/login"> <a className="has-text-link">login </a></Link>
                <div className="block" />
              </span>
              <span className="is-size-5 is-family-code has-text-weight-bold">
                <Link to="/search"> <a className="has-text-link">search </a></Link>
              </span>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
  }

}
