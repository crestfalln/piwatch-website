import http from './http'

interface User {
  UserID: string,
  UserName: string,
  UserPass?: string
}

const checkAll = async () => {
  if (await firstTimeCheck() === true) {
    return { valid: true, to: "/register" }
  }
  return { valid: await userAuth(), to: "/search" }
}

const userAuth = async () => {
  const res = await http.get('/api/auth/');
  if (res.status >= 300)
    return false;
  else
    return true;
};
const getSession = async () => {
  const res = await http.get('/api/auth/');
  if (res.status >= 300)
    return { auth: false, user: { UserID: "", UserName: "" } };
  else {
    const user: User = JSON.parse(res.data);
    return { auth: true, user: user };
  }
};

const firstTimeCheck = async () => {
  const res = await http.get('/api/setup');
  if (res.status === 404)
    return false
  return true
}

const checkAvailability = async () => {
  const res = await http.get('/api/test');
  if (res.status === 200)
    return true;
  return false;
}

export { userAuth, getSession, firstTimeCheck, checkAll, checkAvailability }
