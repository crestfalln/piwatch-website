import LoginForm from './components/loginform'
import React from 'react'
import Footer from './components/footer'
import AbstractRouteMask from './components/routemask'
import { Navigate } from 'react-router-dom';
import { userAuth } from './auth'

export class ProtectedRoutes extends AbstractRouteMask {
  constructor(props: { navigate: string }) {
    super(props);
    this.handler();
  }
  handler = async () => {
    if (await userAuth()) {
      this.setState({ avail: true, ready: true });
    } else {
      this.setState({ avail: false, ready: true });
    }
  }
}
export class NonProtectedRoutes extends AbstractRouteMask {
  constructor(props: { navigate: string }) {
    super(props);
    this.handler();
  }
  handler = async () => {
    if (await userAuth()) {
      this.setState({ avail: false, ready: true });
    } else {
      this.setState({ avail: true, ready: true });
    }
  }
}

export default class LoginPage extends React.Component<{}, { redirect: boolean, redirectTo: string }> {
  mounted: boolean = true;

  loginHandler = () => {
    userAuth().then(res => {
      if (res) {
        this.setState({ redirect: true });
      } else {
      }
    })
  }
  constructor(props: {}) {
    super(props);
    // set it to true first so as not to render login form if already authenticated
    this.state = { redirect: false, redirectTo: "/search" };
    this.loginHandler = this.loginHandler.bind(this);
  }
  componentDidMount() {
    document.title = 'Login.'
    this.mounted = true;
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  render() {
    if (this.state.redirect)
      return (
        <Navigate to={this.state.redirectTo} />
      )
    else {
      return (
        <div id="main">
          <div className="basicLayoutMain">
            <div className="container my-6">
              <div className="field">
                <span className="icon-text ">
                  <span className="icon is-large">
                    <i className="mdi mdi-48px mdi-login " />
                  </span>
                  <span className="is-size-3"> Login </span>
                </span>
              </div>
              <div className="box my-3 mx-3 py-4 px-4">
                <LoginForm ButtonText="Login" submissionHandler={this.loginHandler} />
              </div>
            </div>
          </div>
          <Footer />
        </div>
      )
    }
  }
}
