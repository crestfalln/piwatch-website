import * as Utils from '../utils/utils'
import dayjs, * as Dayjs from 'dayjs'
import CustomParseFormat from 'dayjs/plugin/customParseFormat'
import http from '../http'


export type DatePairList = Utils.pair<Dayjs.Dayjs>[];

// string used to fromat strings to dates
const formatString = ['D/M/YYYY', 'D/M/YY', 'D/MMM', 'D/M', 'D/MMM/YY', 'D/MMM/YYYY', 'D'];
// = ['D', 'D/M', 'D/MMM', 'D/M/YY', 'D/MMM/YY', 'D/M/YY', 'D/M/YYYY', 'D/MMM/YYYY',]

// function that parses inputs and returns a indvidual datelist 
// and a range type datelist with two dates
export const parseInput = (input: string) => {
  // TODO relative dates 
  // TODO better parsing maybe different library? or 
  //      maybe just do it myself

  const first_split = input.split(/\s*(?:,)\s*/);
  const dateList: string[] = [];
  const rangeList: Utils.pair<string>[] = [];

  // this function converts common words like 'today' to date
  // strings
  const convertWords = (word: string) => {
    const today = dayjs().hour(0).minute(0).second(0);
    const yesterday = today.subtract(1, 'day');
    switch (word.toLowerCase()) {
      case 't':
      case 'today':
        return today.format('D/M/YYYY'); case 'y':
      case 'yesterday':
        return yesterday.format('D/M/YYYY');
      default:
        return word;
    }
  };

  first_split.forEach(element => {
    let range_splits = element.split(/\s*(?:-)\s*/);
    if (range_splits.length === 2) {
      if (!range_splits[0] || !range_splits[1])
        throw { msg: "Invalid Enquiry" }
      rangeList.push({ first: range_splits[0], second: range_splits[1] });
    } else if (range_splits.length < 2) {
      if (!element)
        throw { msg: "Invalid Enquiry" }
      dateList.push(element);
    }
  })

  rangeList.forEach((el, index) => {
    let prop: keyof typeof el;
    for (prop in el) {
      rangeList[index][prop] = convertWords(el[prop]);
    }
  });

  dateList.forEach((el, index) => {
    dateList[index] = convertWords(el);
  });
  return { dateList: dateList, rangeList: rangeList };
};

// this function takes indvidual and range date lists and
// returns a list of date pairs to send to the api
export const strToDate = (dateList: string[], rangeList: Utils.pair<string>[]) => {
  dayjs.extend(CustomParseFormat)
  const parser = (element: string) => {
    const parseRes = dayjs(element, formatString)
    if (!parseRes.isValid()) {
      throw { msg: "Invalid Date Format" };
    }
    return parseRes;
  }
  const reqListDates: Utils.pair<Dayjs.Dayjs>[] = [];
  dateList.forEach(element => {
    const parseRes = parser(element);
    reqListDates.push({ first: parseRes, second: parseRes.add(1, 'day') });
  });

  rangeList.forEach((el) => {
    const parseRes1 = parser(el.first);
    const parseRes2 = parser(el.second);
    reqListDates.push({ first: parseRes1, second: parseRes2 });
  });
  return reqListDates;
};

export const getRecordsFromApi = async (dateList: DatePairList) => {
  const apiParams = () => {
    const start: number[] = [];
    const end: number[] = [];
    dateList.forEach(el => {
      start.push(el.first.unix());
      end.push(el.second.unix());
    });
    return { start: JSON.stringify(start), end: JSON.stringify(end) }
  }

  return await http.get('/api/records', {
    params: apiParams()
  })

}
