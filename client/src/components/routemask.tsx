import React from "react";
import { Navigate, Outlet } from "react-router-dom";

export default abstract class AbstractRouteMask extends React.Component<{ navigate: string },
  { avail: boolean, ready: boolean }> {
  abstract handler(): Promise<void>;
  constructor(props: { navigate: string }) {
    super(props);
    this.state = { avail: false, ready: false };
  }
  render() {
    if (!this.state.ready) {
      return (<div className="pageloader is-active"><span className="title">Loading...</span></div>)
    }
    if (this.state.avail) {
      return <Outlet />
    }
    return <Navigate to={this.props.navigate} />
  }
}
