import * as React from "react"
import dayjs from "dayjs"
import timezone from "dayjs/plugin/timezone"
import utc from "dayjs/plugin/utc"

// Set timezone after querying the browser doesn't work on IE 
// but that is fine
dayjs.extend(utc);
dayjs.extend(timezone);
const tz = Intl.DateTimeFormat().resolvedOptions().timeZone
dayjs.tz.setDefault(tz);

export interface record {
  id?: number,
  start: number,
  end: number,
}

// TODO Refactor all this to put computational code in another module

export class ResTableRow extends React.Component<{ rec: record, sr: number }, { rec: record }> {
  public printDateTime(timestamp: number) {
    const datetime = dayjs(timestamp)
    const dateString = datetime.format("DD[th] MMM YYYY");
    const timeString = datetime.format("hh:mm:ss A");
    return {
      date: dateString,
      time: timeString,
      day: datetime.format("dddd"),
      raw: datetime,
    };
  }
  public printTimeDelta(timedelta: number) {
    Math.floor(timedelta);
    timedelta /= 1000;
    let hrs = timedelta / 3600;
    hrs = Math.floor(hrs);
    timedelta %= 3600;
    let mins = timedelta / 60;
    mins = Math.floor(mins);
    timedelta %= 60;
    let secs = timedelta;
    secs = Math.floor(secs)

    return `${hrs.toString() + " hrs" || ""}  
            ${mins.toString() + " mins" || ""}  
            ${secs.toString() + " secs" || ""}  `
  }

  render() {
    const { date, time: start, day } = this.printDateTime(this.props.rec.start);
    const { time: end } = this.printDateTime(this.props.rec.end);
    return (
      <tr>
        <td>{this.props.sr}</td>
        <td>{date}</td>
        <td>{start}</td>
        <td>{end}</td>
        <td>{Math.trunc((this.props.rec.end - this.props.rec.start) / 1000)} s</td>
        <td>{day}</td>
      </tr>
    );
  }
};

export class ResTable extends React.Component<{ recList: record[] }, { recList: record[] }> {
  render() {
    let rows = new Array<JSX.Element>();
    this.props.recList.forEach((el, index) => {
      // JS timestamps are in milliseconds but the server replies in seconds
      // so convert it to milliseconds
      // TODO refactor code to work automatically with seconds
      el.end *= 1000;
      el.start *= 1000;

      rows.push(<ResTableRow rec={el} sr={index + 1} key={index} />)
    })

    return (
      <div className="table-container">
        <table className="table is-fullwidth">
          <thead >
            <tr className="has-background-light">
              <th > Sr. No. </th>
              <th > Date </th>
              <th> Start </th>
              <th> End </th>
              <th> Time Elapsed </th>
              <th> Day </th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>
      </div>
    );
  }
}

