import React, { ReactElement } from 'react'


export interface GenericFormElProps<_P extends GenericFormProps, _S extends GenericFormState> {
  setState: GenericForm<_P, _S>['update']
}

export interface GenericFormProps {
  submissionHandler: () => void,
  ButtonText: string,
}

export interface GenericFormState {
  error: string,
  submission: boolean
}

export default class GenericForm<_P extends GenericFormProps, _S extends GenericFormState>
  extends React.Component<_P, _S> {
  // this string is directly printed below the form so edit this to 
  // get print errors
  badLogin = "";

  // The array that will contain the elements of the form
  formElements: ReactElement<GenericFormElProps<_P, _S>>[] = [];

  // event handler that handles submissions
  submissionHandler(event: React.FormEvent) {
  }
  update<_K extends keyof _S>(state: Pick<_S, _K> | _S | null) {
    this.setState(state);
    console.log("inupdater");
    console.table(state);
  }
  constructor(props: _P) {
    super(props);
    this.submissionHandler = this.submissionHandler.bind(this);
    this.update = this.update.bind(this);
  }
  render() {
    // TODO do this in a more customizable way
    const buttonClass = "button is-success ";
    let buttonFlavor = ""
    if (this.state.submission)
      buttonFlavor = "is-loading"
    const printError = () => {
      return this.state.error
    }
    return (
      <form className="form" onSubmit={this.submissionHandler}>
        {this.formElements}
        <div className="field">
          <p className="control">
            <button className={buttonClass + buttonFlavor}
              type="submit" disabled={this.state.submission}>
              {this.props.ButtonText}
            </button>
          </p>
        </div>
        <div className="field">
          <p className="has-text-danger-dark">
            {
              printError()
            }
          </p>
        </div>
      </form>
    );
  }
}


