import React from 'react'
import { ResTable, record } from './res'
import Search from './search'
import { DatePairList, getRecordsFromApi } from '../processors/search'

interface SearchResWrapState {
  data: record[],
  error: string,
  loading: boolean,
  Auth?: boolean
};

export default class SearchResWrap extends React.Component<{}, SearchResWrapState>{

  constructor(props: {}) {
    super(props);
    this.state = { data: [], error: "", loading: false };
    this.dataHandler = this.dataHandler.bind(this);
    this.errorHandler = this.errorHandler.bind(this);
  }

  errorHandler(error: string) {
    this.setState({ data: [], error: error });
  }

  dataHandler(dateList: DatePairList) {
    this.setState({ loading: true });
    getRecordsFromApi(dateList).then((res) => {
      const parsedData: record[] = JSON.parse(res.data);
      let error = ""
      if(!parsedData.length)
        error = "Found no results for you query."
      this.setState({ data: parsedData, error: error, loading: false })
    }
    );
  };

  render() {
    const printRes = () => {
      if (this.state.data.length) {
        return <ResTable recList={this.state.data} />
      }
      if (this.state.error) {
        return (
          <div className="box has-text-centered has-text-danger-dark 
                          is-size-6 px-2 py-2 mx-4 my-4 ">
            {this.state.error}
          </div>
        );
      }
      return <div />
    };

    return (
      <div className="column">
        <Search dataUpdater={this.dataHandler}
          errorPrinter={this.errorHandler}
          loading={this.state.loading} />
        <div className="container">
          {
            printRes()
          }
        </div>
      </div >
    );
  }
};
