import React from 'react'
import GenericForm, { GenericFormProps, GenericFormState, GenericFormElProps } from './genericForm'
import http from '../http'

// TODO make this better somehow

interface RegisterFormState extends GenericFormState {
  id: string,
  pass: string,
  pass2: string
}

type RegisterFormElProps = GenericFormElProps<GenericFormProps, RegisterFormState> &
  RegisterFormState

// TODO bug with required
type UserIDProps = Pick<RegisterFormElProps, 'setState' | 'submission' | 'id'>
export class UserID extends React.Component<UserIDProps, { val: string }>{
  // Add a mechanism to somehow reflect 
  constructor(props: UserIDProps) {
    super(props);
    this.state = { val: this.props.id };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.props.setState({ id: e.target.value });
    this.setState({ val: e.target.value });

  }
  render() {
    return (
      <div className="field">
        <label className="label">UserID</label>
        <div className="control has-icons-left">
          <input className="input" type="text" placeholder="Enter UserID..."
            required={true}
            disabled={this.props.submission}
            onChange={this.handleChange}
            value={this.state.val}
          />
          <span className="icon is-small is-left">
            <i className="mdi mdi-account"></i>
          </span>
        </div>
      </div>

    )
  }
}
type PasswordProps = Pick<RegisterFormElProps, 'pass' | 'pass2' | 'setState' | 'submission'> &
{ feild: number };
export class Password extends React.Component<PasswordProps, { val: string }>{
  constructor(props: PasswordProps) {
    super(props);
    if (this.props.feild === 0)
      this.state = { val: this.props.pass };
    else
      this.state = { val: this.props.pass2 };
    this.handleChange = this.handleChange.bind(this);
  }
  placeholder() {
    if (this.props.feild === 0) {
      return "Enter Password..."
    }
    return "Enter Password One More Time..."

  }
  label() {
    if (this.props.feild === 0) {
      return "Password"
    }
    return "Confirm Password"
  }
  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ val: e.target.value });
    if (this.props.feild === 0) {
      this.props.setState({ pass: e.target.value });
    }
    else if (this.props.feild === 1) {
      this.props.setState({ pass2: e.target.value });
    }
  }
  render() {
    return (
      <div className="field">
        <label className="label">{this.label()}</label>
        <p className="control has-icons-left">
          <input className="input" type="password" required={true} placeholder={this.placeholder()}
            disabled={this.props.submission}
            onChange={this.handleChange}
            value={this.state.val}
          />
          <span className="icon is-small is-left">
            <i className="mdi mdi-lock"></i>
          </span>
        </p>
      </div>
    )
  }
}

export default class RegisterForm
  extends GenericForm<GenericFormProps, RegisterFormState> {
  constructor(props: GenericFormProps) {
    super(props);
    this.state = { id: "", pass: "", pass2: "", error: "", submission: false, };
    this.submissionHandler = this.submissionHandler.bind(this);
    console.log("inconstructor:");
    this.formElements = [
      <UserID {...this.state} setState={this.update} key={0} />,
      <Password {...this.state} feild={0} setState={this.update} key={1} />,
      <Password {...this.state} feild={1} setState={this.update} key={2} />,
    ]
  }

  submissionHandler = async (event: React.FormEvent) => {
    // set error to nothing in once form has been submitted
    this.setState({ error: "" });


    // errors timesout and disappears after 20 secs
    const errorTimeout = () => { this.setState({ error: "" }); };

    // get the buttons and inputs back in working state
    const buttonTimeout = () => {
      this.setState({
        submission: false, error: this.badLogin
      });
    };
    // Parses Entered password
    const passParser = () => {
      if (this.state.pass.length < 8) {
        throw { msg: "Password must be more than 8 characters", code: -40 };
      }
      if (this.state.pass !== this.state.pass2) {
        throw { msg: "Passwords do not match", code: -41 };
      }
    }

    const putData = async () => {
      console.table(this.state);
      try {
        passParser();
      }
      catch (err: any) {
        this.badLogin = err.msg;
        throw err;
      }
      const res = await http.get('/api/setup', {
        params: {
          userid: this.state.id,
          pass: this.state.pass
        }
      });
      console.log(res.status);
      if (res.status >= 300) {
        if (res.status >= 500) {
          this.badLogin = "Server is not responding properly. Try again later."
        }
        else {
          // this should never happen
          this.badLogin = "Something went wrong somewhere."
        }
        throw { msg: "bad response code", code: -1 };
      }
      this.props.submissionHandler();
      return res;
    }

    event.preventDefault();

    //  set the submission to true this blocks input and button
    this.setState({ submission: true })

    try {
      // wait for token
      await putData();
      this.setState({ error: "Registration Successful", pass: "", pass2: "", submission: false, id: "" });
    } catch (err) {
      // if there is any problems in auth log the error to console
      console.log(err);
      buttonTimeout();
      setTimeout(errorTimeout, 20000);
    }
  }
}
