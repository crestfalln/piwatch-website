import React from 'react'

export default class Footer extends React.Component<{}> {
  render() {
    return (
      <footer className="footer mt-5">
        <div className="content has-text-left mx-5">
          <div>
            <div className="is-size-1 is-bold">
              PiWatch. <br />
            </div>
            <div className="is-size-7">
              Copyright © 2022 Himanshu Gupta. The source code is licensed under
              <a href="https://opensource.org/licenses/GPL-3.0"> GPL version 3</a>.
            </div>
          </div>
        </div>
      </footer>
    )
  }
}
