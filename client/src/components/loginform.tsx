import React from 'react'
import GenericForm, { GenericFormState, GenericFormProps, GenericFormElProps } from './genericForm'
import { Axios } from 'axios'
import http, { HeaderProps } from '../http';
import Cookies from 'js-cookie'

export type LoginFormElProps = GenericFormElProps<GenericFormProps, LoginFormState> & LoginFormState;

export class UserID extends React.Component<LoginFormElProps, { val: string }>{
  constructor(props: LoginFormElProps) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = { val: this.props.id }
  }
  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.props.setState({ id: e.target.value })
    this.setState({ val: e.target.value });
  }
  render() {
    return (
      <div className="field">
        <label className="label">UserID</label>
        <div className="control has-icons-left">
          <input className="input" type="text" placeholder="UserID"
            disabled={this.props.submission}
            onChange={this.handleChange} />
          <span className="icon is-small is-left">
            <i className="mdi mdi-account"></i>
          </span>
        </div>
      </div>

    )
  }
}
export class Password extends React.Component<LoginFormElProps, { val: string }>{
  constructor(props: LoginFormElProps) {
    super(props);
    this.state = { val: this.props.pass }
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.props.setState({ pass: e.target.value });
    this.setState({ val: e.target.value });
  }
  render() {
    return (
      <div className="field">
        <label className="label">Password</label>
        <p className="control has-icons-left">
          <input className="input" type="password" placeholder="Password"
            disabled={this.props.submission}
            onChange={this.handleChange}
            value={this.state.val}
          />
          <span className="icon is-small is-left">
            <i className="mdi mdi-lock"></i>
          </span>
        </p>
      </div>
    )
  }
}
export interface LoginFormState extends GenericFormState {
  id: string,
  pass: string,
}

export default class LoginForm extends
  GenericForm<GenericFormProps, LoginFormState> {

  // event handlet that handles submissions
  submissionHandler = async (event: React.FormEvent) => {

    // set error to nothing in once form has been submitted
    this.setState({ error: "" });

    event.preventDefault();

    // errors timesout and disappears after 20 secs
    const errorTimeout = () => { this.setState({ error: "" }); };

    // get the buttons and inputs back in working state
    const buttonTimeout = () => {
      console.table(this.state);
      console.table(this.badLogin);
      this.setState({
        submission: false, error: this.badLogin
      });
      console.table(this.state);
    };

    // call the api and auth the user, get jwt
    const getToken = async (http: Axios) => {
      // call api for auth
      const res = await http.get('/api/auth/login', {
        params: {
          userid: this.state.id,
          pass: this.state.pass
        }
      });
      console.log(res.status);

      // check if authenticated
      if (res.status >= 300) {
        if (res.status === 403) {
          this.badLogin = "Either username or password is wrong.";
        }
        else if (res.status === 404) {
          this.badLogin = "First time setup has not been done. Go to /register to do it.";
        }
        else if (res.status === 500) {
          this.badLogin = "Server is not responding properly. Try again later."
        }
        else {
          this.badLogin = "Something went wrong."
        }
        throw { msg: "bad response code", code: -1 };
      }

      // if authed change the headers
      http.defaults.headers = { 'x-access-token': res.data } as HeaderProps;

      // do cookies
      Cookies.remove('access-token');
      Cookies.set('access-token', res.data, { expires: 30 });

      // set error to something good for user to see
      this.setState({ error: "Login Successful!" });

      // calls the handler to redirect 
      this.props.submissionHandler();
    }

    //  set the submission to true this blocks input and button
    this.setState({ submission: true })

    try {
      // wait for token
      await getToken(http);

    } catch (err) {
      // if there is any problems in auth log the error to console
      console.log(err);
      console.log("Hello");
      buttonTimeout();
      setTimeout(errorTimeout, 20000);
    }

  }
  constructor(props: GenericFormProps) {
    super(props);
    this.state = { id: "", pass: "", error: "", submission: false, };
    this.formElements = [
      <UserID {...this.state} setState={this.update} />,
      <Password {...this.state} setState={this.update} />,
    ]
  }
}
