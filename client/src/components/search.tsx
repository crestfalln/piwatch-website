import * as React from 'react'
import { parseInput, strToDate, DatePairList } from '../processors/search'


export interface SearchProps {
  dataUpdater: (data: DatePairList) => void
  errorPrinter: (error: string) => void
  loading?: boolean
}
export interface SearchState {
  val: string,
}

export default class Search extends React.Component<SearchProps, SearchState> {

  constructor(props: SearchProps) {
    super(props);
    this.state = { val: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  private inputState() {
    if (this.props.loading)
      return "is-disabled";
    return "";
  }
  //handles enter key
  public handleSubmit() {
    try {
      // parsing input and converting it to sendable list
      const parsedInput = parseInput(this.state.val);
      const reqListDates = strToDate(parsedInput.dateList, parsedInput.rangeList);
      this.props.dataUpdater(reqListDates)
    } catch (error: any) {
      console.log(error);
      if (typeof error.msg === 'string')
        this.props.errorPrinter(error.msg)
      else {
        this.props.errorPrinter("Something went wrong")
      }
    }
    // // Set the input box empty again
    // this.setState({ val: "" });
  }

  public handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ val: event.target.value });
  }
  render() {
    return (
      <div>
        <div className="field">
          <p className="control has-icons-right my-5 mx-2">
            <input className={"input is-medium"}
              disabled={this.props.loading}
              value={this.state.val} onChange={this.handleChange}
              placeholder="Type a single date or a list of dates"
              onKeyPress={(e) => { if (e.key === 'Enter') this.handleSubmit() }} />
            <span className="icon is-medium is-right" >
              <i className="mdi mdi-36px mdi-magnify" />
            </span>
          </p>
        </div>
        <div className="field has-text-right">
          <button className="button is-info is-medium" disabled={this.props.loading}
            onClick={this.handleSubmit}>
            Search
          </button>
        </div>
      </div>
    );
  }
}

