import React from 'react'
import SearchResWrap from './components/searchTable'
import Footer from './components/footer'
import { userAuth } from './auth'

export default class SearchPage extends React.Component<{}, { Auth?: boolean }> {
  componentDidMount() {
    document.title = 'Search.';
  }
  constructor(props: {}) {
    super(props);
    this.state = {};
    userAuth().then(res => {
      this.setState({ Auth: res });
    });
  }
  render() {
    return (
      <div id="main">
        <div className="basicLayoutMain">
          <SearchResWrap />
        </div>
        <Footer />
      </div>
    )
  }
}
