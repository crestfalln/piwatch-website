import app from './app'

const application = app.Instance;

process.on('SIGINT', () => {
  app.quit();
  process.exit();
});
