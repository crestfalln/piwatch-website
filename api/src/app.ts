import express from 'express'
import cors from 'cors'
import db from './db'
import http from 'http'
import routes from './router'

export default class app {
  private static _instance?: app;
  private context: express.Application;
  private server: http.Server;
  private port: string;

  private constructor() {
    this.context = express();
    this.port = process.env.PORT || '2001';
    this.server = this.init(
      async () => {
        console.log(`\x1b[32;1mServer Running on Port\x1b[0m \x1b[34;1m${this.port}\x1b[0m`)
        try {
          await routes.init();
        } catch (error: any) {
          console.error("\x1b[31;1mProblem initializing the api. This should never happen. Hence this program won't even pretend to handle it.\x1b[0m")
          console.trace();
          app.quit();
          process.exit();
        }
      });
  }
  private init(callback: VoidFunction = () => { }) {
    this.context.use(cors());
    return this.context.listen(this.port, callback);
  }
  public static get Instance(): app {
    return this._instance || (this._instance = new this());
  }
  public static get Context() {
    return this.Instance.context;
  }
  public static get Port() {
    return this.Instance.port;
  }
  public static restart() {
    if (!this._instance)
      return;
    if (this._instance.server.listening) {
      this._instance.server.close(() => {
        this._instance = undefined
        this.Instance;
      })
    }
  }
  public static quit() {
    if (!this._instance)
      return;
    console.log("Exiting...");
    db.end();
    this.Instance.server.close();
  }
}
