import bcrypt from 'bcryptjs'
import db, { Queries } from '../db'
import jwt from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'

interface User { UserID: string, UserName: string, UserPass?: string }

export default class auth {
  private key: string;
  static _instance: auth;

  private constructor() {
    if (typeof process.env.main_key != 'string') {
      throw { msg: "public or private key not available", code: -2 }
    }
    this.key = process.env.main_key;
    auth._instance = this;
  }

  static instance = () => {
    return this._instance || new this;
  }
  static publicKey = () => {
    return auth.instance().key;
  }
  private static createToken = (id: string) => {
    return jwt.sign({ id: id }, this.instance().key,
      { expiresIn: 3600 * 24 * 15 });
  }
  public static verifyToken = (token: string, nothrow = true) => {
    try {
      const decode = jwt.verify(token, auth.publicKey());
      if (typeof decode === 'string') {
        if (nothrow) {
          throw { msg: "invalid token", code: -2 }
        }
        return { res: false, decode: null }
      }
      return { res: true, decode: decode.id };
    } catch (error) {
      console.log(error);
      if (!nothrow) {
        throw error;
        // TODO write erro handling code?
      }
      return { res: false, decode: null };
    }
  }
  public static renewToken = async (oldToken: string) => {
    const { res: valid, decode: data } = this.verifyToken(oldToken);
    if (valid) {
      if (data) {
        return this.createToken(data);
      }
    }
  }
  public static userInfo = async (userID: string): Promise<User> => {
    const dbFetch: any[] = await db.exec(Queries.getUserUsingID, [ userID ]);
    if (!dbFetch.length) {
      throw { msg: "invalid userid", code: -1 }
    }
    return { UserID: dbFetch[0].UserID, UserName: dbFetch[0].UserName };
  }
  public static registerUser = async (id: string, password: string) => {
    const pass = await bcrypt.hash(password, 10);
    await db.exec(Queries.insertUser, [id, pass])
  }
  public static authUser = async (id: string, password: string) => {
    const dbFetch: any[] = await db.exec(Queries.getUserUsingID, [ id ] );
    if (!dbFetch.length) {
      throw { msg: "Invalid User ID or Password", code: -1 }
    }
    const passValid = await bcrypt.compare(password, dbFetch[0].UserPass)
    if (passValid) {
      return this.createToken(id)
    } else {
      throw { msg: "Invalid User ID or Password", code: -1 }
    }
  }
};

export const verifyMiddle = (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers['x-access-token'];
  if (!token || typeof token != 'string') {
    return res.status(403).send('Unauthorized');
  }
  try {
    auth.verifyToken(token, false);
  } catch (error) {
    console.log(error);
    return res.status(403).send('Unauthorized');
    //TODO write error handling code
  }
  return next();
}
