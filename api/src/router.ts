import app from './app'
import db, { Queries } from './db'
import Auth, * as auth from './auth/auth'
import { Request, Response, NextFunction } from 'express'

// middleware to disable registeration. 
// a better method might be to just have a second router without 
// /api/setup end point and switch to that instead as it is 
// it causes an extra check at every request
export const disableRegister = (req: Request, res: Response, next: NextFunction) => {
  if (req.path === "/api/setup" && routes.firstTimeSetupDone === true) {
    return res.sendStatus(404);
  }
  else {
    return next();
  }
}

// Connect to database with sufficient timeout as not to crash on slow systems
const connectToDb = async () => {
  for (let i = 0; i < 5; ++i) {
    try {
      console.error(`Trying to connect to database... (Attempt No. ${i + 1})`);
      await db.connect();
      console.error("\x1b[32;1mConnection successful!\x1b[0m");
      break;
    } catch (err: any) {
      if (i == 3) {
        console.error("\x1b[31;1mCannot connect to database.\x1b[0m");
        throw err;
      }
      console.error("\x1b[33;1mCannot connect to database. Retrying in 5 seconds...\x1b[0m");
      await new Promise(res => setTimeout(res, 5000)); // Sleep for like 5 secs 
    }
  }
}

export default class routes {
  static firstTimeSetupLock = false;
  static firstTimeSetupDone = false;
  static badReqErr = { msg: "Bad Request", code: 400 }
  public static async init() {
    try {
      await connectToDb();
      const userCountRes = await db.exec(Queries.getUserCount);
      if (userCountRes[0].count) {
        const userCount = parseInt(userCountRes[0].count)
        if (userCount >= 1) {
          this.firstTimeSetupDone = true;
        }
      }
      else {
        throw { msg: "database not setup properly", code: -3 }
      }
      app.Context.use(disableRegister);
      app.Context.get("/api/errors", auth.verifyMiddle, this.getErrors)
      app.Context.get("/api/records", auth.verifyMiddle, this.getRecs)
      app.Context.get("/api/errors/set", auth.verifyMiddle, this.setError);
      app.Context.get("/api/auth/login", this.authUser);
      app.Context.get('/api/auth/session', this.checkSession);
      app.Context.get('/api/auth/', this.verifySession);
      app.Context.get("/api/setup", this.firstTimeSetup);
      app.Context.get('/api/test', (req, res) => { return res.sendStatus(200); })
      app.Context.all("*", this.notfound)
    } catch (error: any) {
      console.table(error);
      throw error;
    }
  }

  private static notfound = (req: Request, res: Response) => {
    res.sendStatus(404);
  }

  private static firstTimeSetup = async (req: Request, res: Response) => {
    // TODO use post instead of get
    try {
      console.table(this.firstTimeSetupLock);
      if (this.firstTimeSetupLock === true) {
        return res.sendStatus(409);
      }
      this.firstTimeSetupLock = true;
      // request validation
      if (!(req.query && req.query.userid && req.query.pass)) {
        throw this.badReqErr;
      }
      if (!(typeof req.query.userid === 'string' &&
        typeof req.query.pass === 'string')) {
        throw this.badReqErr;
      }
      try {
        await Auth.registerUser(req.query.userid, req.query.pass);
        this.firstTimeSetupDone = true;
        return res.status(200).send(req.query.userid);
      } catch (err: any) {
        // TODO more error handling
        if (err.code && err.msg &&
          typeof err.code === 'number' &&
          typeof err.msg === 'string') {
          if (err.code === -1) {
            await new Promise((resolve) => { setTimeout(resolve, 2000) })
          }
          else if (err.code === -3) {
            throw { msg: err.msg, code: 502 }
          }
        }
        console.log(err);
        throw { msg: err.msg, code: 403 }
      }
      // TODO Error type definations?
    } catch (err: any) {
      this.firstTimeSetupLock = false;
      console.log(err);
      if (err.msg && err.code && typeof err.code === 'number' &&
        typeof err.msg === 'string') {
        return res.status(err.code).send(err.msg);
      }
      else {
        return res.status(500).send("Internal Server Error");
      }
    }
  }
  private static getErrors = async (req: Request, res: Response) => {
    let bad = true;
    if (req.query) {
      if (req.query.start && typeof req.query.start === "string" &&
        req.query.end && typeof req.query.end === "string") {
        bad = false;
        const End = new Date(parseInt(req.query.end));
        const Start = new Date(parseInt(req.query.start));
        try {
          if (req.query.seen && typeof req.query.seen === "string") {
            const Seen = Boolean(req.query.seen);
            res.send(await db.exec(Queries.errorQueryPart, [Start, End, Seen]));
          }
          else if (!req.query.seen) {
            res.send(await db.exec(Queries.errorQuery, [Start, End]))
          }
        } catch (error) { console.log(error); bad = true; }
      }
    }
    if (bad) {
      res.statusCode = 400;
      res.send("Bad Request")
    }
  }
  private static setError = async (req: Request, res: Response) => {
    // TODO use put instead of get
    if (req.query) {
      if (req.query.state && typeof req.query.state === "string") {
        const state = Boolean(req.query.state);
        if (req.query.ids && typeof req.query.ids === "string") {
          const ids = JSON.parse(req.query.ids);
          if (Array.isArray(ids)) {
            ids.map((el) => {
              try {
                db.exec(Queries.modErrQuery, [state, parseInt(el)])
                res.sendStatus(200);
              } catch (error) {
                console.log(error);
                res.sendStatus(404);
              }
            });
          }
        }
      }
    }
  }
  private static authUser = async (req: Request, res: Response) => {
    try {
      // request validation
      if (!(req.query && req.query.userid && req.query.pass)) {
        throw this.badReqErr;
      }
      if (!(typeof req.query.userid === 'string' &&
        typeof req.query.pass === 'string')) {
        throw this.badReqErr;
      }
      try {
        const token = await Auth.authUser(req.query.userid, req.query.pass);
        res.send(token);
      } catch (err: any) {
        // TODO more error handling
        if (err.code && err.msg &&
          typeof err.code === 'number' &&
          typeof err.msg === 'string') {
          if (err.code === -1) {
            await new Promise((resolve) => { setTimeout(resolve, 2000) })
          }
          else if (err.code === -3) {
            throw { msg: err.msg, code: 502 }
          }
        }
        console.log(err);
        throw { msg: err.msg, code: 403 }
      }
      // TODO Error type definations?
    } catch (err: any) {
      console.log(err);
      if (err.msg && err.code && typeof err.code === 'number' &&
        typeof err.msg === 'string') {
        res.status(err.code).send(err.msg);
      }
      else {
        res.status(500).send("Internal Server Error");
      }
    }
  }
  private static checkSession = async (req: Request, res: Response) => {
    const token = req.headers['x-access-token'];
    if (token && typeof token === 'string') {
      const { res: validity, decode: decode } = Auth.verifyToken(token);
      if (validity) {
        res.send(await Auth.userInfo(decode));
      }
      else {
        res.status(403).send("Unauthorized");
      }
    }
  }
  private static verifySession = async (req: Request, res: Response) => {
    const token = req.headers['x-access-token'];
    if (token && typeof token === 'string') {
      const { res: validity } = Auth.verifyToken(token);
      if (validity) {
        res.send(true);
      } else {
        res.status(403).send("Unauthorized")
      }
    } else {
      res.status(403).send("header not found")
    }
  }
  private static getRecs = async (req: Request, res: Response) => {
    try {
      // request validation
      if (!(req.query && req.query.start && req.query.end &&
        typeof req.query.start === 'string' &&
        typeof req.query.end === 'string')) {
        throw this.badReqErr;
      }

      // request parsing
      const Start = JSON.parse(req.query.start);
      const End = JSON.parse(req.query.end);

      // more validation
      if (!(Array.isArray(Start) && Array.isArray(End)
        && Start.length == End.length)) {
        throw this.badReqErr;
      }

      // even more validation
      let valid = true;
      for (let index = 0; index < Start.length; ++index) {
        if (typeof Start[index] != 'number' || typeof End[index] != 'number') {
          valid = false;
          break;
        }
      }
      if (!valid) {
        throw this.badReqErr;
      }

      // making request to database and storing as promises
      const promises: Promise<any>[] = [];
      for (let index = 0; index < Start.length; ++index) {
        promises.push(db.exec(Queries.recsQuery, [Start[index], End[index]]));
      }

      // function to resolve above said promises
      const getData = async () => {
        const toSend: any[] = [];
        for (let index = 0; index < promises.length; ++index) {
          const arr = await promises[index];
          arr.forEach((element: any) => {
            toSend.push(element);
          });
        }
        return toSend;
      }

      //resolving promises and sending data back
      try {
        res.send(await getData());
      } catch (error) { console.log(error); throw { err: "Internal Server Error", code: 500 }; }
    }

    // Catching errors and sending back appropriate codes
    catch (err: any) {
      try {
        res.statusCode = err.code;
        res.send(err.err);
        // handling unforseen errors if shit goes wilder than excpected
      } catch (error) {
        console.log(error);
        res.statusCode = 500;
        res.send("Something went wrong")
      }
    };
  }
}
