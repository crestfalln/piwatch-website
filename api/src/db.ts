import pg from 'pg'

enum Queries {
  getUserUsingToken =
  `SELECT * FROM "Users"
     WHERE "Token" = $1`
  ,
  getUserUsingID =
  `SELECT * FROM "Users"
     WHERE "UserName" = $1`
  ,
  getUserCount =
  `SELECT COUNT(*) FROM "Users"`
  ,
  insertUser =
  `INSERT INTO "Users"
      ("UserName", "UserPass")
   VALUES($1, $2)`
  ,
  insertUserWithToken =
  `INSERT INTO "Users"
      ("UserName", "UserPass", "Token")
   VALUES($1, $2, $3)`
  ,
  errorQuery =
  `SELECT * FROM "Errors" 
     WHERE "time" BETWEEN $1 AND $2`
  ,
  errorQueryPart =
  `SELECT * FROM "Errors"
     WHERE "time" BETWEEN $1 AND $2
     AND "read" = $3 `
  ,
  modErrQuery =
  `UPDATE "Errors"
     SET "read" = $1 
     WHERE "id" = $2 `
  ,
  recsQuery =
  `SELECT * FROM "Recs"
     WHERE "start" BETWEEN $1 AND $2
     ORDER BY "start" ASC`
  ,
}

export default class db {

  private static _instance: db;
  private pool: pg.Pool;
  private constructor() {
    // console.table(process.env)
    this.pool = new pg.Pool()
  }
  private static get instance() {
    return this._instance || (this._instance = new this());
  }
  public static async exec(query: Queries, data?: any) {
    try {
      const res = await db.instance.pool.query(query, data);
      //console.table(res.rows);
      return res.rows;
    } catch (error) { console.log(error); throw error; }
  }
  public static async connect() {
    await this.instance.pool.connect();
  }
  public static end() {
    this.instance.pool.end();
  }
}

export { Queries };
