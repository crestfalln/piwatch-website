const path = require('path')

module.exports = {
  target: 'node',
  mode: 'production',
  entry: './src/index.ts', // make sure this matches the main root of your code
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    path: path.join(__dirname, 'bundle'), // this can be any path and directory you want
    filename: 'bundle.js',
  },
  optimization: {
    minimize: true, // enabling this reduces file size and readability
  },
}
